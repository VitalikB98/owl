(function ($, Drupal) {
  /*global jQuery:false */
  /*global Drupal:false */
  "use strict";

  /**
   * Provide Bootstrap select theming.
   */
  Drupal.behaviors.bootstrapSelectTheme = {
    attach: function (context) {
      var $context = $(context);

      /* здесь наш js */
        $('.selectpicker').selectpicker({
          style: 'btn-primary',
        }); 
    }
  };

})(jQuery, Drupal);

