$(document).ready(function(){
  $('.our-team-block').owlCarousel({
	    loop:true,
	    margin:10,
	    nav:false,
	    touchDrag:true,
	    responsive:{
	        0:{
	            items:1
	        },
	        600:{
	            items:3
	        },
	        1000:{
	            items:5
	        }
	    }
	});
   
});

