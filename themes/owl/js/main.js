
$(document).ready(function () {
	
	$('li.search').click(function () {
		$('.block-search-form-block').toggle();
	});

	$('.team-wrapper').on('mouseover', function () {
		$(this).addClass('active-img');
		//$('.social').show();
	});

	$('.team-wrapper').on('mouseout', function () {
		$(this).removeClass('active-img');
		//$('.social').hide();
	});

	function change_footer () {
		var footer_height = $('footer').height();
		$('.body-page').css({
			"margin-bottom" : footer_height+20
		});
	}
	

	
	change_footer();
	$(window).resize(change_footer());


	var navigation = $('.navbar-header');

	var currentScrollTop = 0;

	$(window).scroll(function(){

		currentScrollTop = $(window).scrollTop();

		if (currentScrollTop >= $('#header-head').height()) {
			navigation.fadeIn("slow", function() {
   				$(this).addClass("navbar-active navbar-fixed-top");
			});	

		} else  {	
			navigation.removeClass('navbar-active navbar-fixed-top');
		}	
	});


	function tabsActivation() {
	    $('.form-radio:checked').parent().css({
	    	"background-color" : "#202020",
	    	"color" : "white"
	    });  
	}
	tabsActivation();
	$(window).mouseover(function () {
		tabsActivation();
	});
});

